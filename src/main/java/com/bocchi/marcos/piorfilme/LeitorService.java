package com.bocchi.marcos.piorfilme;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LeitorService implements ApplicationRunner {
    private final PiorFilmeService piorFilmeService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        piorFilmeService.deleteAll();

        Reader reader = Files.newBufferedReader(Paths.get("src/main/resources/static/movielist.csv"));

        CsvToBean<PiorFilme> csvToBean = new CsvToBeanBuilder<PiorFilme>(reader)
                .withType(PiorFilme.class)
                .withSeparator(';')
                .build();

        List<PiorFilme> filmes = csvToBean.parse();

        piorFilmeService.save(filmes);

        var f = piorFilmeService.findMinMaxIntervalWinner();
    }
}
