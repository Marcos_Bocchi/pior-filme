package com.bocchi.marcos.piorfilme;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PiorFilmeRepository extends JpaRepository<PiorFilme, Long> {

    List<PiorFilme> findByWinner(boolean winner);
}
