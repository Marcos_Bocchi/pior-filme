package com.bocchi.marcos.piorfilme;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PiorFilmeServiceImpl implements PiorFilmeService {

    private final PiorFilmeRepository repository;

    @Override
    public void save(List<PiorFilme> pioresFilmes) {
        repository.saveAll(pioresFilmes);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public IntervaloTo findMinMaxIntervalWinner() {
        var filmesVencedores =  repository.findByWinner(true);
        HashMap<String, List<Integer>> produtores = mapearProdutores(filmesVencedores);

        ArrayList<ProdutorTo> produtoresVencedores = carregarProdutores(produtores);

        return carregarIntervalo(produtoresVencedores);
    }

    private HashMap<String, List<Integer>> mapearProdutores(List<PiorFilme> filmesVencedores) {
        var produtoresVencedores = new HashMap<String, List<Integer>>();
        filmesVencedores.forEach(piorFilme -> {
            var p = piorFilme.getProducers().replace(" and", ",").split(", ");
            for (String s : p) {
                if (!produtoresVencedores.containsKey(s)) {
                    produtoresVencedores.put(s, new ArrayList<>());
                }
                produtoresVencedores.get(s).add(piorFilme.getYear());
            }
        });
        return produtoresVencedores;
    }

    private ArrayList<ProdutorTo> carregarProdutores(HashMap<String, List<Integer>> produtores) {
        var produtoresVencedores = new ArrayList<ProdutorTo>();

        produtores.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .forEach(entry -> {
                    entry.getValue().sort(Integer::compareTo);
                    for (var i = 1; i < entry.getValue().size(); i++) {
                        var produtor = new ProdutorTo();
                        produtor.setProducer(entry.getKey());
                        produtor.setPreviousWin(entry.getValue().get(i-1));
                        produtor.setFollowingWin(entry.getValue().get(i));
                        produtoresVencedores.add(produtor);
                    }
                });
        return produtoresVencedores;
    }

    private IntervaloTo carregarIntervalo(ArrayList<ProdutorTo> produtoresVencedores) {
        var intervalos = produtoresVencedores.stream().collect(Collectors.groupingBy(ProdutorTo::getInterval));
        var min= intervalos.entrySet().stream().min(Map.Entry.comparingByKey()).get();
        var max= intervalos.entrySet().stream().max(Map.Entry.comparingByKey()).get();

        var intervalo = new IntervaloTo();
        intervalo.setMin(min.getValue());
        intervalo.setMax(max.getValue());

        return intervalo;
    }

}

