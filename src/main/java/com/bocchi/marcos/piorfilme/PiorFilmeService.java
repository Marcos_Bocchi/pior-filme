package com.bocchi.marcos.piorfilme;

import java.util.List;

public interface PiorFilmeService {
    void save(List<PiorFilme> pioresFilmes);

    void deleteAll();

    IntervaloTo findMinMaxIntervalWinner();
}
