package com.bocchi.marcos.piorfilme;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pior-filme")
public class PiorFilmeController {

    private final PiorFilmeService service;

    @GetMapping("/find-min-max-interval-winner")
    public IntervaloTo findMinMaxIntervalWinner() {
        return service.findMinMaxIntervalWinner();
    }


}
