package com.bocchi.marcos.piorfilme;

import lombok.Data;

import java.util.List;

@Data
public class IntervaloTo {
    private List<ProdutorTo> min;
    private List<ProdutorTo> max;
}
