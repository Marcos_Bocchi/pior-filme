package com.bocchi.marcos.piorfilme;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Getter
@Setter
@Entity
@Table(name="PIOR_FILME")
public class PiorFilme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "YEAR")
    @CsvBindByName
    private Integer year;

    @Column(name = "TITLE")
    @CsvBindByName
    private String title;

    @Column(name = "STUDIOS")
    @CsvBindByName
    private String studios;

    @Column(name = "PRODUCERS")
    @CsvBindByName
    private String producers;

    @Column(name = "WINNER")
    @CsvCustomBindByName(converter = StringToBoolean.class)
    private boolean winner;

}
