package com.bocchi.marcos.piorfilme;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class StringToBoolean extends AbstractBeanField<String> {

    @Override
    protected Object convert(String campo) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        return "yes".equals(campo);
    }
}
