package com.bocchi.marcos.piorfilme;

import lombok.Data;

@Data
public class ProdutorTo {

    private String producer;
    private int interval;
    private Integer previousWin;
    private Integer followingWin;

    public int getInterval() {
        return followingWin - previousWin;
    }
}

