package com.bocchi.marcos.piorfilme;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
class PiorFilmeTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void teste() {
        ResponseEntity<IntervaloTo> response = this.testRestTemplate
                .exchange("/pior-filme/find-min-max-interval-winner", HttpMethod.GET, null, IntervaloTo.class);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(1, response.getBody().getMin().size());
        Assertions.assertEquals(1, response.getBody().getMax().size());
        Assertions.assertEquals("Joel Silver", response.getBody().getMin().get(0).getProducer());
        Assertions.assertEquals("Matthew Vaughn", response.getBody().getMax().get(0).getProducer());
        Assertions.assertEquals(1, response.getBody().getMin().get(0).getInterval());
        Assertions.assertEquals(13, response.getBody().getMax().get(0).getInterval());
    }
}
