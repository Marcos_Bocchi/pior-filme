# Pior Filme

### Descrição do Projeto
Carrega uma lista de filmes indicados na categoria Pior Filme do Golden Raspberry Awards.
e permite consultar o produtor com maior intervalo entre dois prêmios consecutivos, e o que
obteve dois prêmios mais rápido.

### 🎲 Rodando o Back End (api)

```bash
# Clone este repositório
$ git clone <https://github.com/tgmarinho/nlw1>

# Acesse a pasta do projeto no terminal/cmd
$ cd pior-filme

# Instale as dependências
$ mvn clean package -DskipTests=true

# Execute a aplicação
$ java -jar target\pior-filme-1.0.0-SNAPSHOT.jar

# O servidor inciará na porta:8080 - acesse <http://localhost:8080/pior-filme/find-min-max-interval-winner>
```

### 🎲 Rodando os testes
```bash
# Execute os teste
$ mvn test
```

